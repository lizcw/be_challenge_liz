from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
import os
import csv
import pytz
from datetime import datetime
from .models import Shift, Employee, Roster
def index(request):
    return HttpResponse('Welcome the Simple Roster Application');

class IndexView(generic.ListView):
    template_name ='index.html'
    context_object_name = 'datalist'

    def get_shift_count(self):
        return Shift.objects.count()

    def get_employee_count(self):
        return Employee.objects.count()

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['shift_list']= self.get_shift_count()
        context['employee_list'] = self.get_employee_count()
        return context

    def get_queryset(self):
        """Return all rosters"""
        return Roster.objects.all()

########################################################################

def create_roster(request):
    roster = Roster.objects.create(timezone=pytz.UTC)
    roster.save()
    response = '<p><a href="/roster">Return</a></p><p>Roster created: %d</p>' % roster.id
    print(response)
    return HttpResponse(response)

def load_shifts(request):
    shifts_data = 'data/shifts.csv'
    roster = Roster.objects.first()
    with open(os.path.join(settings.STATIC_ROOT, shifts_data)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader, None)
        for row in csv_reader:
            startdate = datetime.strptime(row[0] + ' ' + row[1], '%d/%m/%Y %I:%M:%S %p')
            enddate = datetime.strptime(row[0] + ' ' + row[2], '%d/%m/%Y %I:%M:%S %p')
            print('start: ', startdate)
            print('end:', enddate)
            shift = Shift.objects.create(start_shift=startdate.replace(tzinfo=pytz.UTC),
                                         end_shift=enddate.replace(tzinfo=pytz.UTC),
                                         roster_id=roster)
            shift.save()

    response = '<p><a href="/roster">Return</a></p><p>Shift data loaded: %d</p>' % Shift.objects.count()
    print(response)
    return HttpResponse(response)

def load_employees(request):
    emp_data = 'data/employees.csv'
    roster = Roster.objects.first()
    with open(os.path.join(settings.STATIC_ROOT, emp_data)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader, None)
        for row in csv_reader:
            emp = Employee.objects.create(first_name=row[0],
                                         last_name=row[1],
                                         status='active')
            emp.save()

    response = '<p><a href="/roster">Return</a></p><p>Employee data loaded: %d</p>' % Employee.objects.count()
    print(response)
    return HttpResponse(response)

