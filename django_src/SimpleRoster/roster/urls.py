from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create', views.create_roster, name="roster_create"),
    path('shifts', views.load_shifts, name="load_shifts"),
    path('employees', views.load_employees, name="load_employees"),
]