from django.db import models

class Roster(models.Model):
    id = models.AutoField(primary_key=True)
    timezone = models.CharField(max_length=120)

class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    status = models.CharField(max_length=30)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def totalShiftDays(self):
        hours = 0
        for s in self.shift_set.objects.all():
            period = (s.end_date - s.start_date)
            hours += period.hours
        return hours / 24

    def hasShift(self, shift):
        try:
            rtn = (self.shift_set.get(shift).objects.count() > 0)
        except Shift.DoesNotExist:
            rtn = False
        except Shift.MultipleObjectsReturned:
            rtn = True
        return rtn

    def hasAdjacentShift(self, shift):
        try:
            pre = self.shift_set.filter(employees__shift__end_shift__exact=shift.start_shift)
            post = self.shift_set.filter(employees__shift__start_shift__exact=shift.end_shift)
            rtn = pre.count() > 0 or post.count() > 0
        except Shift.MultipleObjectsReturned:
            rtn = True
        return rtn

    def isAvailable(self, shift):
        if self.shift_set.count() > 0:
            is_available = not self.hasShift(shift)
            if is_available:
                is_available = not self.hasAdjacentShift(shift)
        else:
            is_available = self.totalShiftDays() <= 20
        return is_available


    def __str__(self):
        return self.first_name + ' ' + self.last_name

class Shift(models.Model):
    id = models.AutoField(primary_key=True)
    roster_id = models.ForeignKey(Roster, on_delete=models.CASCADE)
    start_shift = models.DateTimeField()
    end_shift = models.DateTimeField()
    employees = models.ManyToManyField(Employee)

    def hasEmployees(self):
        return self.employees.count() > 0

    def __str__(self):
        return self.id
